//
//  ViewController.swift
//  WhereAmI
//
//  Created by Austin Brown on 2/8/19.
//  Copyright © 2019 Austin Brown. All rights reserved.
//  https://www.youtube.com/watch?v=WPpaAy73nJc

import UIKit
import MapKit
import CoreLocation

class MapScreen: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    let locationManager = CLLocationManager()
    let regionInMeters = 10000.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CheckLocationServices()
        // Below was done in storyboard
        //mapView.delegate = self
    }
    
    func SetupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func CheckLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            SetupLocationManager()
            CheckLocationAuthorization()
        } else {
            // Show alert
        }
    }
    
    func CheckLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            // Puts blue dot on map
            mapView.showsUserLocation = true
            CenterViewOnUserLocation()
            // Updates location as the user moves
            locationManager.startUpdatingLocation()
            break
        case .denied:
            // Show alert how to turn on permission
            break
        case .notDetermined:
            // Haven't hit allow or not allow
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            // Show alert
            break
        case .authorizedAlways:
            // Always pulling data
            break
        }
    }
    
    func CenterViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
    }
}

extension MapScreen: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {return}
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion.init(center: center, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
        mapView.setRegion(region, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        CheckLocationAuthorization()
    }
}

